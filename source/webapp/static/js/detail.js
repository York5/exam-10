const baseUrl = 'http://localhost:8000/';


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function getFullPath(path) {
    path = path.replace(/^\/+|\/+$/g, '');
    path = path.replace(/\/{2,}/g, '/');
    return baseUrl + path;
}

let addPrivateForm, addPrivateUserInput, myFileID;

function setUpGlobalVars() {
    addPrivateForm = $('#private_users_form');
    addPrivateUserInput = $('#user_input');
    myFileID = $('#my_file_id');
}

function makeRequest(path, method, data=null) {
    let settings = {
        url: getFullPath(path),
        method: method,
        dataType: 'json',
        headers: {'X-CSRFToken': getCookie('csrftoken')},
    };
    if (data) {
        settings['data'] = JSON.stringify(data);
        settings['contentType'] = 'application/json';
    }
    return $.ajax(settings);
}

function setUpAddPrivate() {
    addPrivateForm.on('submit', function (event) {
        event.preventDefault();
        let request = makeRequest(
            'file/add_private_user',
            'post',
            {"username": addPrivateUserInput.val(), "file_id": myFileID.val()});
        request.done(function (data, status, response) {
            console.log('User Added');
        }).fail(function (response, status, message) {
            console.log('Could not add User');
            console.log(response.responseText);
        });
    });
}

    // createLink.on('click', function(event) {
    //     event.preventDefault();
    //
    //     formSubmit.on('click', function(event) {
    //         createForm.submit();
    //     });
    // });
// }

$(document).ready(function() {
    setUpGlobalVars();
    setUpAddPrivate();
    console.log('I have loaded');
});

