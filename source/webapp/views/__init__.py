from .file_views import IndexView, FileView, FileCreateView, FileUpdateView, FileDeleteView
from .json_views import AddPrivateAccess