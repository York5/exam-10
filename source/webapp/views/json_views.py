from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.generic.base import View

from webapp.models import File


class AddPrivateAccess(View):
    def post(self, request):
        username = request.POST["username"]
        user = User.objects.get(username=username)
        file_id = request.POST["file_id"]
        file = File.objects.get(pk=file_id)
        if not user:
            return JsonResponse({'Message':"User not found"}, status=400)
        elif user in file.user_access.all():
            return JsonResponse({'Message': "User already has private access"}, status=400)
        else:
            File.user_access.add(user)
            return JsonResponse({'Message':"User now has private access to this file"}, data=request.data)
