from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.db.models import Q
from urllib.parse import urlencode
from webapp.forms import SimpleSearchForm, FileForm, AnonymousFileForm
from webapp.models import File


class IndexView(ListView):
    template_name = 'index.html'
    context_object_name = 'files'
    queryset = File.objects.filter(access='public')
    ordering = ['-created_at']
    paginate_by = 10
    paginate_orphans = 1

    def get(self, request, *args, **kwargs):
        self.form = SimpleSearchForm(data=self.request.GET)
        self.search = None
        if self.form.is_valid():
            self.search_value = self.form.cleaned_data['search']
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.search_value:
            queryset = queryset.filter(
                Q(name__icontains=self.search_value)
            )
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['form'] = self.form
        if self.search_value:
            context['query'] = urlencode({'search': self.search_value})
        return context


class FileView(DetailView):
    template_name = 'file.html'
    model = File


class FileCreateView(CreateView):
    form_class = AnonymousFileForm
    model = File
    template_name = 'create.html'

    def get_success_url(self):
        return reverse('webapp:file_view', kwargs={'pk': self.object.pk})

    def get_form(self, form_class=None):
        # file = self.get_file()
        if self.request.user.is_authenticated:
            form_class = FileForm
        return super(FileCreateView, self).get_form(form_class)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            self.object = form.save()
            if request.user.is_authenticated:
                self.object.author = request.user
            # else:
            #     self.object.access = 'public'
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class FileUpdateView(PermissionRequiredMixin, UpdateView):
    model = File
    template_name = 'update.html'
    form_class = FileForm
    context_object_name = 'file'
    permission_denied_message = 'Access Denied!'
    permission_required = 'webapp.change_file'

    def has_permission(self):
        file = File.objects.get(pk=self.kwargs['pk'])
        if file.author == self.request.user or super().has_permission():
            return True

    def get_form(self, form_class=None):
        file = self.get_file()
        if file.author is None or not self.request.user.is_authenticated:
            form_class = AnonymousFileForm
        return super(FileUpdateView, self).get_form(form_class)

    def get_file(self):
        file_pk = self.kwargs.get('pk')
        return get_object_or_404(File, pk=file_pk)

    def get_success_url(self):
        return reverse('webapp:file_view', kwargs={'pk': self.object.pk})


class FileDeleteView(PermissionRequiredMixin, DeleteView):
    model = File
    template_name = 'delete.html'
    context_object_name = 'photo'
    success_url = reverse_lazy('webapp:index')
    permission_denied_message = 'Access Denied!'
    permission_required = 'webapp.delete_photo'

    def has_permission(self):
        file = File.objects.get(pk=self.kwargs['pk'])
        if file.author == self.request.user or super().has_permission():
            return True