from django import forms

from webapp.models import File


class SimpleSearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label='Search')


class FileForm(forms.ModelForm):

    class Meta:
        model = File
        exclude = ['created_at', 'author', 'user_access']


class AnonymousFileForm(forms.ModelForm):

    class Meta:
        model = File
        exclude = ['created_at', 'author', 'user_access', 'access']