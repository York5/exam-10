from django.contrib.auth.models import User
from django.db import models

PUBLIC = 'public'
ACCESS_CHOICES = [(PUBLIC, 'Public'), ('hidden', 'Hidden'), ('private', 'Private')]


class File(models.Model):
    file = models.FileField(null=True, blank=True, upload_to='files', verbose_name='File')
    name = models.CharField(max_length=200, verbose_name='File Name')
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Author',
                               related_name='files')
    access = models.CharField(max_length=40, null=False, blank=False, verbose_name='Access', default=PUBLIC,
                              choices=ACCESS_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Date Added')
    user_access = models.ManyToManyField(User, related_name='private_files', blank=True,  verbose_name='User_Access')

    def __str__(self):
        return self.name



