# Generated by Django 2.2 on 2020-03-14 06:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0005_auto_20200314_0448'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='name',
            field=models.CharField(max_length=200, verbose_name='File Name'),
        ),
    ]
