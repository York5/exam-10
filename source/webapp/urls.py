from django.urls import path
from webapp.views import IndexView, FileView, FileCreateView, FileUpdateView, FileDeleteView, AddPrivateAccess

app_name = 'webapp'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('file/<int:pk>/', FileView.as_view(), name='file_view'),
    path('file/add/', FileCreateView.as_view(), name='file_add'),
    path('file/<int:pk>/update/', FileUpdateView.as_view(), name='file_update'),
    path('file/<int:pk>/delete/', AddPrivateAccess.as_view(), name='file_delete'),

    path('file/add_private_user', AddPrivateAccess.as_view(), name='add_private_access')

    # path('project/<int:pk>/add-review/', ReviewForProductCreateView.as_view(), name='product_review_create'),
    # path('review/<int:pk>/update/', ReviewUpdateView.as_view(), name='review_update'),
    # path('review/<int:pk>/delete/', ReviewDeleteView.as_view(), name='review_delete'),
    ]

